import processing.serial.*;

Serial myPort;  // The serial port

PFont f;                           // STEP 1 Declare PFont variable

float r = 30;
float turn = 0;
float goldenR = (1 - sqrt(5))/2;
float h = 100;
int value = 1024;
boolean noWater = false;
String plantState = "";
void setup() {
  //fullScreen();
  size(700, 1000);
  background(0);
   
   f = createFont("Arial",16,true); // STEP 2 Create Font
   
   //printArray(Serial.list());
   
  // Open the port you are using at the rate you want:
  myPort = new Serial(this, Serial.list()[0], 9600);
}

void draw() {
  
  // Read String from serial
  if (myPort.available() > 0) {
    String inBuffer = myPort.readStringUntil(13);   
    if (inBuffer != null) {
     
      inBuffer = inBuffer.replace('\n', ' ').trim();
      value = int(inBuffer);
    }
      fill(0);
     rect(80, 80, 500, 150);
     
    textFont(f,16);                  // STEP 3 Specify font to be used
    fill(0,100,0);                         // STEP 4 Specify font color 
    
   if(value>900){
     plantState = "Plant NEEDS WATERING";
     noWater=true;
   }else{
     noWater = false;
     if((600<value)&&(value<900)){
      plantState = "Plant is DRY";
     }else{
      plantState = "Plant is HUMID";
    
    }
   }
    textAlign(CENTER);
    text(plantState,180,100);   // STEP 5 Display Text
  }
  
  turn += goldenR;
  float alphaBlatt = random(100);
  float alphaRand = random(100, 150);

  translate(width/2, height/2-h);

  noFill();
   if(noWater){
     stroke(120,120, 120, 120);
   }else{
     stroke(0, random(255), random(150), random(100));
   }
  
  bezier(0, 0+(height/2)+h, 0, random(0, 300), 0, random(0, 300), random(-300, 300), random(0, 350));

  stroke(0, alphaRand);
   rotate(turn);
  if(noWater){
    fill(120, 120, 120, 0);
  }else{
    
    fill(random(200, 255), 0, random(150, 255), alphaBlatt);
  
  }
 
  ellipse(0, 0, 50, random(100, 300));
  fill(0);
  ellipse(0, 0, 2*r, 2*r);

}

void keyPressed() {
  if (key == 's') {
    myPort.clear();
    myPort.write("1");
  }
}
